<?php
/**
 * @link https://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license https://www.yiiframework.com/license/
 */

namespace giisky\Advertising;

use Yii;
use yii\base\BootstrapInterface;

class Module extends \yii\base\Module implements BootstrapInterface
{

    public $controllerNamespace = 'giisky\Advertising\Controllers';

    public function bootstrap($app)
    {
    }
}
