<?php

/**
 * @link https://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license https://www.yiiframework.com/license/
 */

namespace giisky\Advertising\Controllers;

use Yii;
use backend\controllers\Controller;

class TestController extends Controller
{

    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['except'] = [
            'index'
        ];
        return $behaviors;
    }

    public function actionIndex()
    {
        echo '我是测试模块';
    }
}
