<?php

/**
 * @link https://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license https://www.yiiframework.com/license/
 */

namespace giisky\Advertising\Controllers;

use Yii;
use backend\controllers\Controller;

class AdvertisingController extends Controller
{

    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['except'] = [
            'index'
        ];
        return $behaviors;
    }

    public function actionIndex()
    {
        // 1. 我修复了一个bug
        // 2. 我修复了第二个bug
        // 3. 我修复了第三个bug
        // 4. 我修复了第四个bug
        // 5. 我修复了第五个bug
        // 6. 我修复了第六个bug-code_o
        // 6. 我修复了第六个bug-code_t
        $abc = [];
        foreach ($abc as $key => $value) {

            if ($value == '1') {
                echo 'hello';
                echo 'hello-two';
                echo 'hello';
                echo 'hello-two';
                echo 'hello-two';
                echo 'hello';
            } else {
                echo 'world';
                echo 'world-two';
                echo 'world-two';
                echo 'two-two';
                echo 'world-two';
            }

            if ($key == 0) {
                echo 'aaaa';
                echo 'aaaa-one';
                echo 'aaaa-one';
                echo 'aaaa';
            } else {
                echo 'bbbb';
                echo 'bbbb';
                echo 'bbbb-one';
                echo 'bbbb-one';
                echo '222222222';
            }
        }
        echo '我是第二个模块-two';
        echo '再次测试-two';
        echo '第三次测试咯-two';
        echo '第四四次测试咯-three-three-three-three';
        // 记录
        die;
    }
}
